# Versions variables
export ZLIBVER=1.2.11
export MPICHVER=3.3.2
export HDF5VER=1.12.0
export HDF5PATH=$(echo $HDF5VER | tr . _)
export NETCDFCVER=c-4.7.4
export NETCDFFORTVER=fortran-4.5.2
export JASPERVER=1.900.1
export LIBPVER=1.6.37
export WRFVER=4.2.2
export WPSVER=4.2

#choices
export WRFCHOICE=34
export WPSCHOICE=3

#Static variables
export DLDIR=/tmp/wrf-sources
export WRFDIR=/opt/Build_WRF
export TESTDIR=$DLDIR/TEST
export LIBDIR=$WRFDIR/LIBRARIES
export NETCDF=$LIBDIR/netcdf
export CC="gcc"
export CXX="g++"
export FC="gfortran"
export FCFLAGS="-m64"
export F77="gfortran"
export FFLAGS="-m64"
export HDF5=$LIBDIR/hdf5
export WRF_DIR=$WRFDIR/WRF-$WRFVER
export JASPERLIB=$LIBDIR/grib2/lib
export JASPERINC=$LIBDIR/grib2/include
export LIBS="-lnetcdf -lhdf5_hl -lhdf5 -lz -lcurl"
export PATH=$LIBDIR/hdf5/bin:$NETCDF/bin:$LIBDIR/mpich/bin:$PATH
export LD_LIBRARY_PATH="$LIBDIR/hdf5/lib:$LIBDIR/grib2/lib:$NETCDF/lib"
export LDFLAGS="-L$LIBDIR/hdf5/lib -L$LIBDIR/netcdf/lib -L$LIBDIR/grib2/lib"
export CPPFLAGS="-I$LIBDIR/hdf5/include -I$LIBDIR/netcdf/include -I$LIBDIR/grib2/include"


yum install epel-release -y
yum update -y
yum install -y file time m4 which libpng-devel gcc-gfortran netcdf-fortran netcdf-fortran-devel cpp gcc gcc-c++ wget csh perl git make
mkdir $DLDIR $TESTDIR $WRFDIR
cd $TESTDIR

wget https://www2.mmm.ucar.edu/wrf/OnLineTutorial/compile_tutorial/tar_files/Fortran_C_tests.tar
mkdir Fortran_C_tests
tar -xf Fortran_C_tests.tar -C Fortran_C_tests
cd Fortran_C_tests

# running tests, if one fails, type CTRL-C and enjoy debugging :)
gfortran TEST_1_fortran_only_fixed.f
./a.out
echo "Excpeted: \
    SUCCESS test 1 fortran only fixed format"
# pwd
#read

gfortran TEST_2_fortran_only_free.f90
./a.out
echo "Expected: \
Assume Fortran 2003: has FLUSH, ALLOCATABLE, derived type, and ISO C Binding \
SUCCESS test 2 fortran only free format"
# pwd
#read

gcc TEST_3_c_only.c
./a.out
echo "Expected: \
SUCCESS test 3 c only"
# pwd
#read

gcc -c -m64 TEST_4_fortran+c_c.c
gfortran -c -m64 TEST_4_fortran+c_f.f90
gfortran -m64 TEST_4_fortran+c_f.o TEST_4_fortran+c_c.o
./a.out
echo "Expected: \
C function called by Fortran \
Values are xx = 2.00 and ii = 1 \
SUCCESS test 4 fortran calling c"
# pwd
#read

./TEST_csh.csh
echo "Expected: \
    SUCCESS csh test"
# pwd
#read

./TEST_perl.pl
echo "Expected: \
    SUCCESS perl test"
# pwd
#read

./TEST_sh.sh
echo "Expected: \
SUCCESS sh test"
# pwd
#read

# #libraries building
cd $DLDIR
# mkdir LIBRARIES
#hdf5
wget https://hdf-wordpress-1.s3.amazonaws.com/wp-content/uploads/manual/HDF5/HDF5_$HDF5PATH/source/hdf5-$HDF5VER.tar.gz
#mpich
wget http://www.mpich.org/static/downloads/$MPICHVER/mpich-$MPICHVER.tar.gz
#netcdfC
wget ftp://ftp.unidata.ucar.edu/pub/netcdf/netcdf-$NETCDFCVER.tar.gz
#netcdfFort
wget ftp://ftp.unidata.ucar.edu/pub/netcdf/netcdf-$NETCDFFORTVER.tar.gz
#jasper
wget http://repository.timesys.com/buildsources/j/jasper/jasper-$JASPERVER/jasper-$JASPERVER.tar.gz
#libpng
wget http://prdownloads.sourceforge.net/libpng/libpng-$LIBPVER.tar.gz
#zlib
wget https://zlib.net/zlib-$ZLIBVER.tar.gz

#zlib
tar xzvf zlib-$ZLIBVER.tar.gz   #or just .tar if no .gz present
cd zlib-$ZLIBVER
./configure --prefix=$LIBDIR/grib2
make
make install
# pwd
echo "expected location displayed: $DLDIR/zlib-$ZLIBVER"
echo "zlib should be installed in $LIBDIR/grib2"
cd $DLDIR
#read

#hdf5
tar -xzf hdf5-$HDF5VER.tar.gz
cd hdf5-$HDF5VER
./configure --with-zlib=$LIBDIR/grib2 --prefix=$LIBDIR/hdf5 --enable-shared
make
make install
# pwd
echo "expected location displayed: $DLDIR/hdf5-$HDF5VER"
echo "hdf5 should be installed in $LIBDIR/hdf5"
cd $DLDIR
read

#libpng
tar xzvf libpng-$LIBPVER.tar.gz   #or just .tar if no .gz present
cd libpng-$LIBPVER
./configure --prefix=$LIBDIR/grib2 LDFLAGS=$LDFLAGS CPPFLAGS=$CPPFLAGS
make
make install
# pwd
echo "expected location displayed: $DLDIR/libpng-$LIBPVER"
echo "libpng should be installed in $LIBDIR/grib2"
cd $DLDIR
#read

#jasper
tar xzvf jasper-$JASPERVER.tar.gz    #or just .tar if no .gz present
cd jasper-$JASPERVER
./configure --prefix=$LIBDIR/grib2
make
make install
# pwd
echo "expected location displayed: $DLDIR/jasper-$JASPERVER"
echo "jasper should be installed in $LIBDIR/grib2"
cd $DLDIR
#read



#netcdfc
tar xzvf netcdf-$NETCDFCVER.tar.gz  #or just .tar if no .gz present
cd netcdf-$NETCDFCVER
./configure --prefix=$NETCDF --disable-dap --enable-netcdf-4 --enable-shared
make
make install
make check
ls -lash $NETCDF/include
# pwd
echo "expected location displayed: $DLDIR/netcdf-$NETCDFCVER"
echo "netcdf-c should be installed in $LIBDIR/netcdf"
cd $DLDIR
ls -lash $LIBDIR/netcdf/include
read

#netcdfFort
tar xzvf netcdf-$NETCDFFORTVER.tar.gz
cd netcdf-$NETCDFFORTVER
./configure --prefix=$NETCDF --disable-dap --enable-shared
make
make install
make check
echo "expected location displayed: $DLDIR/netcdf-$NETCDFFORTVER"
echo "netcdf-fortran should be installed in $LIBDIR/netcdf"
echo "mpich compilation coming, this may take a few minutes"
ls -lash $LIBDIR/netcdf/include
cd $DLDIR
read

#mpich
tar xzvf mpich-$MPICHVER.tar.gz    #or just .tar if no .gz present
cd mpich-$MPICHVER
./configure --prefix=$LIBDIR/mpich
make
make install
make check
echo $PATH
echo $PATH
# pwd
echo "mpich location displayed: $DLDIR/mpich-$MPICHVER"
echo "mpich should be installed in $LIBDIR/mpich"
cd $DLDIR
# read

#Library compatibility test, hope it won't crash too
wget https://www2.mmm.ucar.edu/wrf/OnLineTutorial/compile_tutorial/tar_files/Fortran_C_NETCDF_MPI_tests.tar
tar -xf Fortran_C_NETCDF_MPI_tests.tar

echo "Testing libraries"
# Fortran + C + NetCDF
cp -Rf ${NETCDF}/include/netcdf.inc .
gfortran -c 01_fortran+c+netcdf_f.f
gcc -c 01_fortran+c+netcdf_c.c
gfortran 01_fortran+c+netcdf_f.o 01_fortran+c+netcdf_c.o \
     -L${NETCDF}/lib -lnetcdff -lnetcdf
./a.out
echo "Expected: \
C function called by Fortran \
Values are xx = 2.00 and ii = 1 \
SUCCESS test 1 fortran + c + netcdf"
# pwd
# read

#  Fortran + C + NetCDF + MPI
cp ${NETCDF}/include/netcdf.inc .
mpif90 -c 02_fortran+c+netcdf+mpi_f.f
mpicc -c 02_fortran+c+netcdf+mpi_c.c
mpif90 02_fortran+c+netcdf+mpi_f.o \
02_fortran+c+netcdf+mpi_c.o \
     -L${NETCDF}/lib -lnetcdff -lnetcdf
mpirun ./a.out
echo "Expected: \
C function called by Fortran \
Values are xx = 2.00 and ii = 1 \
status = 2 \
SUCCESS test 2 fortran + c + netcdf + mpi"
# pwd
# read


cd $WRFDIR
#WRF install
wget https://github.com/wrf-model/WRF/archive/v$WRFVER.tar.gz
tar -xzf v$WRFVER.tar.gz
mv -f v$WRFVER WRF-$WRFVER
cd WRF-$WRFVER
echo -n $WRFCHOICE | ./configure
echo "This may take some time, get a coffe and relax"
./compile em_real >& log.compile
ls -lash main | grep .exe
echo "Expected if
-real case:
    wrf.exe (model executable)
    real.exe (real data initialization)
    ndown.exe (one-way nesting)
    tc.exe (for tc bogusing--serial only) 
-idealized:
    wrf.exe (model executable)
    ideal.exe (ideal case initialization) 
"
cd $WRFDIR
# pwd
# read

wget https://github.com/wrf-model/WPS/archive/v$WPSVER.tar.gz
tar -xzf v$WPSVER.tar.gz
mv -f v$WPSVER WPS-$WPSVER
cd WPS-$WPSVER
./clean

echo "You should be given a list of various options for compiler types, whether to compile in serial or parallel, and whether to compile ungrib with GRIB2 capability. Unless you plan to create extremely large domains, it is recommended to compile WPS in serial mode, regardless of whether you compiled WRF in parallel. It is also recommended that you choose a GRIB2 option (make sure you do not choose one that states 'NO_GRIB2'). You may choose a non-grib2 option, but most data is now in grib2 format, so it is best to choose this option. You can still run grib1 data when you have built with grib2."
echo -n $WPSCHOICE | ./configure
echo "This may take a few minutes"
./compile >& log.compile

ls -lash geogrid/src/ | grep .exe

echo "Expected:
geogrid.exe -> geogrid/src/geogrid.exe
ungrib.exe -> ungrib/src/ungrib.exe
metgrid.exe -> metgrid/src/metgrid.exe "
# pwd
# read



