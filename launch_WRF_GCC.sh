# You should give a look at slurm documentation and change these parameters
#SBATCH --job-name=wrf-test
#SBATCH --output:wrf-test.log
#SBATCH --ntasks=96
#SBATCH --cpus-per-task=1
#SBATCH --nodes=3
#SBATCH --ntasks-per-nodes=32
#SBATCH --ntasks-per-socket=16
#SBATCH --mem-per-cpu=4096
#SBATCH --time=06:00:00

export WRFVER=4.2.2
export WPSVER=4.2
export WRFDIR=/opt/Build_WRF
export LIBDIR=$WRFDIR/LIBRARIES
export NETCDF=$LIBDIR/netcdf
export CC="gcc"
export CXX="g++"
export FC="gfortran"
export FCFLAGS="-m64"
export F77="gfortran"
export FFLAGS="-m64"
export HDF5=$LIBDIR/hdf5
export WRF_DIR=$WRFDIR/WRF-$WRFVER
export JASPERLIB=$LIBDIR/grib2/lib
export JASPERINC=$LIBDIR/grib2/include
export LIBS="-lnetcdf -lhdf5_hl -lhdf5 -lz -lcurl"
export PATH=$LIBDIR/hdf5/bin:$NETCDF/bin:$LIBDIR/mpich/bin:$PATH
export LD_LIBRARY_PATH="$LIBDIR/hdf5/lib:$LIBDIR/grib2/lib:$NETCDF/lib"
export LDFLAGS="-L$LIBDIR/hdf5/lib -L$LIBDIR/netcdf/lib -L$LIBDIR/grib2/lib"
export CPPFLAGS="-I$LIBDIR/hdf5/include -I$LIBDIR/netcdf/include -I$LIBDIR/grib2/include"


cd $WRFDIR/WPS-$WPSVER
./geogrid.exe >& ./geogrid.log
tail geogrid.log
echo "geogrid should be done"
#to delete after first try to avoid having to interact at each step
echo -e "waiting for user input...\ntype CTRL+C to leave if you see an error in las log"
read
###################################################################


ln -sf ./ungrib/Variable_Tables/Vtable.GFS Vtable
./link_grib.csh $WRFDIR/data/GFS0p5/2020022500/


./ungrib.exe >& ./ungrib.log
tail ungrib.log
echo "ungrib should be done"
#to delete after first try to avoid having to interact at each step
echo -e "waiting for user input...\ntype CTRL+C to leave if you see an error in las log"
read
###################################################################


./metgrid.exe >& ./metgrid.log
tail metgrib.log
echo "metgrib should be done"
#to delete after first try to avoid having to interact at each step
echo -e "waiting for user input...\ntype CTRL+C to leave if you see an error in las log"
read
###################################################################


cd $WRF_DIR/run
ln -sf $WRFDIR/WPS-$WPSVER/met_em.d01.* .
./real.exe >& ./real.log
tail real.log
echo "real should be done"
#to delete after first try to avoid having to interact at each step
echo -e "waiting for user input...\ntype CTRL+C to leave if you see an error in las log"
read
###################################################################


echo `date`
srun ./wrf.exe >& ./wrf.log
echo `date`
tail wrf.log
echo "wrf should be done"
echo "This script is finished"
